require 'test_helper'

class DataTablesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get data_tables_index_url
    assert_response :success
  end

  test "should get create" do
    get data_tables_create_url
    assert_response :success
  end

  test "should get show" do
    get data_tables_show_url
    assert_response :success
  end

end
