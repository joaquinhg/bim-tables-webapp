Rails.application.routes.draw do
  get 'home/index'

  root to: 'home#index'
  devise_for :users, path: '/'

  namespace :api do
    resources :data_schemas, path: 'schemas', only: %i(index create show update)
    resources :data_tables, only: %i(create show) do
      member do
        get 'url'
      end
    end

    resources :revision_sets, only: %i() do
      collection do
        get 'project/:project_key', to: 'revision_sets#project'
      end
    end
  end

  namespace :client do
    resources :data_schemas, path: 'schemas', only: %i(new edit)
    resources :data_table_views, only: %i(show)
    resources :projects, only: %i(show)
  end

  resources :downloads, only: %i() do
    collection do
      get 'plugin/:code', to: 'downloads#plugin'
      get 'installer/:release', to: 'downloads#installer'
      get 'token/:release', to: 'downloads#token'
    end
  end

  resources :share_links, path: 'shares', only: %i() do
    member do
      get 'schedule'
      get 'revision/:number', to: 'share_links#revision'
    end
  end

  namespace :dashboard do
    resources :dashboards, path: '/', only: %i(index)
  end

  namespace :admin do
    resources :plugin_bundles, path: 'plugins', only: %i(index new create show)
    resources :plugin_installers, path: 'installers', only: %i(index new create show)
    resources :users, only: %i(index)
  end

  if Rails.env == "development"
    get '/document/categories', to: 'emulator#categories'
    get '/document/families', to: 'emulator#families'
    get '/document/instance-parameters', to: 'emulator#instance_parameters'
    get '/document/type-parameters', to: 'emulator#type_parameters'
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
