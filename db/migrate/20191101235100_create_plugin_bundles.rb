class CreatePluginBundles < ActiveRecord::Migration[6.0]
  def change
    create_table :plugin_bundles do |t|
      t.string :release
      t.string :version
      t.string :assetfile
      t.string :content_type

      t.timestamps
    end
  end
end
