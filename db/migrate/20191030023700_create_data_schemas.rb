class CreateDataSchemas < ActiveRecord::Migration[6.0]
  def change
    create_table :data_schemas do |t|
      t.string :custom_id
      t.string :name
      t.json :data

      t.timestamps
    end
  end
end
