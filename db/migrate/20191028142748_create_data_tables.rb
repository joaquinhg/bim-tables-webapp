class CreateDataTables < ActiveRecord::Migration[6.0]
  def change
    create_table :data_tables do |t|
      t.string :project_key
      t.string :model_filename
      t.string :name
      t.string :datafile

      t.timestamps
    end
  end
end
