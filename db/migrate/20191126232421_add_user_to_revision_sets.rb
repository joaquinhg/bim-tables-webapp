class AddUserToRevisionSets < ActiveRecord::Migration[6.0]
  def change
    add_reference :revision_sets, :user, foreign_key: true
  end
end
