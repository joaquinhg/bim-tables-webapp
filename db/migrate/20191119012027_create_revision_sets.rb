class CreateRevisionSets < ActiveRecord::Migration[6.0]
  def change
    create_table :revision_sets do |t|
      t.string :project_key, null: false
      t.string :grouping_id, null: false
      t.string :uuid, null: false

      t.timestamps
    end
  end
end
