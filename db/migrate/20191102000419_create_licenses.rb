class CreateLicenses < ActiveRecord::Migration[6.0]
  def change
    create_table :licenses do |t|
      t.references :user, null: false, foreign_key: true
      t.string :code
      t.string :release
      t.boolean :active

      t.timestamps
    end
  end
end
