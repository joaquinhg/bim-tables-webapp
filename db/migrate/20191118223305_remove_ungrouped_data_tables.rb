class RemoveUngroupedDataTables < ActiveRecord::Migration[6.0]
  def up
    say_with_time "Removing ungrouped data_tables" do
      DataTable.where(grouping_id: nil).destroy_all      
    end
  end
end
