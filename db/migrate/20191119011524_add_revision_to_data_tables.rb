class AddRevisionToDataTables < ActiveRecord::Migration[6.0]
  def change
    add_column :data_tables, :revision, :integer
  end
end
