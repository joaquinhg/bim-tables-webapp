class AddRevisionSetToDataTables < ActiveRecord::Migration[6.0]
  def change
    add_reference :data_tables, :revision_set, null: true, foreign_key: true
  end
end
