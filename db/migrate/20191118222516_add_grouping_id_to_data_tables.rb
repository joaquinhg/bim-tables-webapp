class AddGroupingIdToDataTables < ActiveRecord::Migration[6.0]
  def change
    add_column :data_tables, :grouping_id, :string
  end
end
