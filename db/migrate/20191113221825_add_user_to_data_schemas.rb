class AddUserToDataSchemas < ActiveRecord::Migration[6.0]
  def change
    add_reference :data_schemas, :user, foreign_key: true
  end
end
