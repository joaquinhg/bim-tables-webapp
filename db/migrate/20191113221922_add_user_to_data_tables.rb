class AddUserToDataTables < ActiveRecord::Migration[6.0]
  def change
    add_reference :data_tables, :user, foreign_key: true
  end
end
