class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable, :lockable, :trackable

  # Associations
  has_many :licenses
  has_many :data_schemas
  has_many :data_tables
  has_many :revision_sets

  def default_license
    DefaultLicenseCreator.new(user: self).create_if_missing!
  end
end
