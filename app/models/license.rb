class License < ApplicationRecord
  # Associations
  belongs_to :user

  # Validations
  validates :code, uniqueness: true

  # Scopes
  scope :latest, -> { unscope(:order).order(created_at: :desc) }
  scope :active, -> { where(active: true) }
end
