class DataTable < ApplicationRecord
  # Associations
  belongs_to :user
  belongs_to :revision_set

  mount_uploader :datafile, DatafileUploader

  # Scopes
  scope :latest, -> { unscope(:order).order(revision: :desc) }
end
