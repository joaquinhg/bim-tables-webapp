class RevisionSet < ApplicationRecord
  extend FriendlyId

  # Associations
  belongs_to :user
  has_many :data_tables

  has_one :latest, -> { order(revision: :desc) }, class_name: 'DataTable'

  friendly_id :uuid
end
