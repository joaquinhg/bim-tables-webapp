class PluginInstaller < ApplicationRecord
  mount_uploader :assetfile, AssetfileUploader

  # Scopes
  scope :latest, -> { unscope(:order).order(created_at: :desc) }
end
