import React from 'react';
import Tabulator from 'tabulator-tables';
import Distribution from 'components/data/Distribution';

class ViewData extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.populateTable(this.props.data);
  }

  name() {
    return "view-" + this.props.view.Name;
  }

  populateTable(data) {
    if (data == null) return;
    if (data.Instances == null) return;

    var instances = new Tabulator("#" + this.name(), {
      data: this.computeData(data),
      layout:"fitColumns",      //fit columns to width of table
      responsiveLayout:"hide",  //hide columns that dont fit on the table
      tooltips:true,            //show tool tips on cells
      addRowPos:"top",          //when adding a new row, add it to the top of the table
      history:true,             //allow undo and redo actions on the table
      // pagination:"local",       //paginate the data
      // paginationSize:20,         //allow 7 rows per page of data
      movableColumns:true,      //allow column order to be changed
      resizableRows:true,       //allow row order to be changed
      initialSort:[             //set the initial sort order of the data
        {column:"count", dir:"desc"},
      ],
      groupStartOpen: false,
      columns: this.getColumns(),
    });
  }

  computeData(data) {
    var dist = new Distribution(data, this.props.view.Distribution);
    return dist.getRows();
  }

  getColumns() {
    var cols = [];
    this.props.view.Distribution.Parameters.forEach(function (p){
      var item = { title: p.Name, field: p.Name };
      cols.push(item);
    });

    cols.push({ title: "Count", field: "count" });

    return cols;
  }

  render() {
    return <div id={this.name()}>Computing...</div>;
  }
}

export default ViewData
