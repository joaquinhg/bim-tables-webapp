import React from 'react';
import { Paper } from '@material-ui/core';
import PropTypes from "prop-types";
import TableDataBreadcrumbs from 'components/TableDataBreadcrumbs';
import TableDataShareButton from 'components/TableDataShareButton';

class TableDataHeader extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Paper>
        <TableDataBreadcrumbs data_table={this.props.data_table} />
        <span>(Rev {this.props.data_table.revision})</span>
        <TableDataShareButton data_table_id={this.props.data_table.id} />
      </Paper>
    )
  }
}

TableDataHeader.propTypes = {
  data_table: PropTypes.object
};

export default TableDataHeader
