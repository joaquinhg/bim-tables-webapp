import React from 'react';
import { Box } from '@material-ui/core';
import { Share } from '@material-ui/icons';
import PropTypes from "prop-types";
import TableDataHeader from 'components/TableDataHeader';
import TableData from 'components/TableData';

class TableDataView extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Box>
        <TableDataHeader data_table={this.props.data_table} />
        <TableData data_table={this.props.data_table} />
      </Box>
    )
  }
}

TableDataView.propTypes = {
  data_table: PropTypes.object
};

export default TableDataView
