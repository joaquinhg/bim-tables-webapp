import axios from 'axios';

class BimTablesApi {
  constructor(baseurl, token) {
    this.baseurl = baseurl;
    this.client = axios.create({
        baseUrl: baseurl,
        headers: { 'Authorization': 'Bearer ' + token }
    });
  }

  url(action) {
    let r = this.baseurl + action;
    return r;
  }

  async getDataTableUrl(id) {
    return await this.client.get('/api/data_tables/' + id + '/url');
  }

  async getScheduleRevision(share_uuid, number) {
    return await this.client.get(`/shares/${share_uuid}/revision/${number}`);
  }
}

export default new BimTablesApi("", typeof gon === 'undefined' ? '' : gon.token);
