import React from 'react';
import { Box, MenuItem, InputLabel } from '@material-ui/core';
import { Share } from '@material-ui/icons';
import Select from 'react-select';
import PropTypes from "prop-types";
import TableDataBreadcrumbs from 'components/TableDataBreadcrumbs';
import TableData from 'components/TableData';

import BimTablesApi from 'components/BimTablesApi';

class RevisionSetView extends React.Component {
  constructor(props) {
    super(props);

    this.state = { currentRevision: props.current_data };
  }

  revisions = () => {
    return this.props.revision_ids.map(rev => this.option(rev.revision));
  }

  option = (number) => {
    return ({
      value: number,
      label: `Rev ${number}`,
    });
  }

  handleChange = (option) => {
    BimTablesApi.getScheduleRevision(this.props.revision_set.uuid, option.value)
      .then((resp) => this.setState({ currentRevision: resp.data }));
  }

  render() {
    return (
      <Box>
      <TableDataBreadcrumbs data_table={this.props.current_data} />
        <InputLabel id="revision-label">Revision</InputLabel>
        <Select
          labelId="revision-label"
          options={this.revisions()}
          defaultValue={this.option(this.state.currentRevision.revision)}
          onChange={this.handleChange}
        >
        </Select>
        <TableData key={this.state.currentRevision.id} data_table={this.state.currentRevision} />
      </Box>
    )
  }
}

RevisionSetView.propTypes = {
  revision_set: PropTypes.object,
  revision_ids: PropTypes.array,
  current_data: PropTypes.object,
};

export default RevisionSetView
