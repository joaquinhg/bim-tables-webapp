import React from 'react';
import PropTypes from "prop-types";
import { Paper, TextField, Button } from '@material-ui/core';
import axios from 'axios';
import { useFormik } from 'formik';
import SaveIcon from '@material-ui/icons/Save';

import RevitApi from 'packs/tables';
import RevitClientApi from 'components/revit/RevitClientApi';
import MultiSelect from 'components/MultiSelect';

class DataSchema extends React.Component {
  constructor(props) {
    super(props);

    this.selectOptions = this.selectOptions.bind(this);
    this.sortData = this.sortData.bind(this);
    this.getObjects = this.getObjects.bind(this);
    this.getDataSchema = this.getDataSchema.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onNameChange = this.onNameChange.bind(this);
    this.onCategoryChange = this.onCategoryChange.bind(this);
    this.onTypeParameterChange = this.onTypeParameterChange.bind(this);
    this.onInstanceParameterChange = this.onInstanceParameterChange.bind(this);

    this.state = {
      schema: props.schema,
      categories: [],
      instance_parameters: [],
      type_parameters: [],
      selectedCategories: this.selectOptions(props.schema.data.Categories),
      selectedTypeParameters: this.selectOptions(props.schema.data.TypeParameters),
      selectedInstanceParameters: this.selectOptions(props.schema.data.InstanceParameters),
    };
  }

  componentDidMount() {
    var gets = [];

    gets.push(RevitApi.getCategories());
    gets.push(RevitApi.getTypeParameters());
    gets.push(RevitApi.getInstanceParameters());

    Promise.all(gets)
    .then(values => {
      this.setState({
        categories: this.sortData(values[0].data),
        type_parameters: this.sortData(values[1].data),
        instance_parameters: this.sortData(values[2].data),
      });
    });
  }

  selectOptions(items) {
    return items.map(item => ({
      value: item.Id,
      label: item.Name,
    }));
  }

  sortData(items) {
    return items.sort((a, b) => { return a.Name.localeCompare(b.Name); } );
  }

  getObjects(selectValue, collection) {
    let objs = [];

    selectValue.forEach(option => {
      let o = collection.find(item => { return item.Id == option.value; });
      if (o != null) {
        objs.push(o);
      }
    });
    return objs
    // return selectValue.map(item => ({ Id: item.value, Name: item.label }));
  }

  getDataSchema() {
    let schema = this.state.schema;
    schema.data.Categories = this.getObjects(this.state.selectedCategories, this.state.categories);
    schema.data.TypeParameters = this.getObjects(this.state.selectedTypeParameters, this.state.type_parameters);
    schema.data.InstanceParameters = this.getObjects(this.state.selectedInstanceParameters, this.state.instance_parameters);

    return schema;
  }

  handleSubmit(e) {
    e.preventDefault();
    let dataSchema = this.getDataSchema();

    switch(this.props.method) {
      case "post":
        axios.post(this.props.url, { data_schema: dataSchema }).then(resp => { window.location.replace(resp.data.url); });
        break;

      case "put":
        axios.put(this.props.url, { data_schema: dataSchema }).then(resp => { alert("Saved"); });
        break;

      default:
        alert("Unknown request method");
    }
  }

  onNameChange(e) {
    let schema = this.state.schema
    schema.name = e.target.value;
    this.setState({ schema: schema });
  }
  onCategoryChange(values) {
    this.setState({ selectedCategories: values });
  }

  onTypeParameterChange(values) {
    this.setState({ selectedTypeParameters: values });
  }

  onInstanceParameterChange(values) {
    this.setState({ selectedInstanceParameters: values });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <TextField
          id="name"
          name="name"
          required
          label="Name"
          defaultValue={this.state.schema.name}
          margin="normal"
          variant="outlined"
          onChange={this.onNameChange}
        />
        <MultiSelect
          label="Categories"
          placeholder="Type or select categories"
          items={this.selectOptions(this.state.categories)}
          value={this.state.selectedCategories}
          onChange={this.onCategoryChange}
          isMulti={true}
        />
        <MultiSelect
          label="Type Parameters"
          placeholder="Type or select parameters"
          items={this.selectOptions(this.state.type_parameters)}
          value={this.state.selectedTypeParameters}
          onChange={this.onTypeParameterChange}
          isMulti={true}
        />
        <MultiSelect
          label="Instance Parameters"
          placeholder="Type or select parameters"
          items={this.selectOptions(this.state.instance_parameters)}
          value={this.state.selectedInstanceParameters}
          onChange={this.onInstanceParameterChange}
          isMulti={true}
        />
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
        >
          Save
        </Button>
      </form>
    );
  }
}

DataSchema.propTypes = {
  url: PropTypes.string,
  method: PropTypes.string,
  schema: PropTypes.object,
};

export default DataSchema
