import React from 'react';
import { Box, TextField, MenuItem } from '@material-ui/core';
import PropTypes from "prop-types";

import MultiSelect from 'components/MultiSelect';
import TableData from 'components/TableData';

class RevisionSetBrowser extends React.Component {
  constructor(props) {
    super(props);

    this.state = { selectedValue: null, tableComponent: null };
  }

  handleItemSelect = (item) => {
    this.setState({
      selectedValue: item.value,
      tableComponent: this.table(item.value),
    });
  }

  menuItems = (revision_sets) => {
    return revision_sets.map(rs => ({
      value: rs.id,
      label: rs.latest.name,
    }));
  }

  getRevisionSet = (id) => {
    return this.props.revision_sets.find(rs => rs.id == id);
  }

  table = (id) => {
    if (id != null) {
      let rs = this.getRevisionSet(id);
      return <TableData key={rs.id} data_table={rs.latest} />;
    }

    return null;
  }

  render() {
    return (
      <Box>
        <MultiSelect
          label="Schedules"
          placeholder="Type or select schedule"
          items={this.menuItems(this.props.revision_sets)}
          value={this.state.selectedValue}
          onChange={this.handleItemSelect}
          isMulti={false}
        />
        {this.state.tableComponent}
      </Box>
    )
  }
}

RevisionSetBrowser.propTypes = {
  revision_sets: PropTypes.array,
};

export default RevisionSetBrowser
