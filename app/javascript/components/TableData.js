import React from 'react';
import PropTypes from "prop-types";
import { Tab, Tabs, Paper, AppBar, Breadcrumbs, Link, Badge } from '@material-ui/core';
import { TableChart } from '@material-ui/icons';
import Tabulator from 'tabulator-tables';
import TableInstances from 'components/TableInstances';
import TableTypes from 'components/TableTypes';
import ViewData from 'components/ViewData';

class TableData extends React.Component {
  constructor(props) {
    super(props);

    this.state = { current_tab: 0, data: null };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    fetch(this.props.data_table.datafile.url)
    .then((resp) => resp.json())
    .then((json) => this.setState({ data: json }));
  }

  handleChange(event, value) {
    this.setState({ current_tab: value });
  }

  render() {
    if (this.state != null && this.state.data != null) {
      let table = <div></div>;

      if (this.state.current_tab == 0) {
        table = <TableInstances data={this.state.data} />;
      }
      else if (this.state.current_tab == 1) {
        table = <TableTypes data={this.state.data} />;
      }
      else if (this.state.current_tab == 2) {
        table = <ViewData data={this.state.data} view={this.state.data.Schema.Views[0]} />;
      }

      return (
        <Paper>
          <Tabs value={this.state.current_tab} onChange={this.handleChange}>
            <Tab value={0} label="Instances" />
            <Tab value={1} label="Types" />
          </Tabs>
          {table}
        </Paper>
      )
    }
    else {
      return <div>Loading...</div>
    }
  }
}

TableData.propTypes = {
  data_table: PropTypes.object
};

export default TableData
