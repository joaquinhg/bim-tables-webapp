import React from 'react';
import { Button, Popover, TextField } from '@material-ui/core';
import { Share, FileCopy } from '@material-ui/icons';
import PropTypes from "prop-types";
import CopyToClipboard from 'react-copy-to-clipboard';
import axios from 'axios';

import BimTablesApi from 'components/BimTablesApi';

class TableDataShareButton extends React.Component {
  constructor(props) {
    super(props);

    this.state = { anchorElement: null, url: null };
  }

  handleClick = (event) => {
    var target = event.currentTarget;

    BimTablesApi.getDataTableUrl(this.props.data_table_id)
    .then(resp => {
      this.setState({ anchorElement: target, url: resp.data.url });
    });
  }

  handleClose = () => {
    this.setState({ anchorElement: null });
  }

  render() {
    let open = this.state.anchorElement != null;

    return (
      <span>
        <Button onClick={this.handleClick}>
          <Share fontSize="small"/>
        </Button>
        <Popover
          open={open}
          anchorEl={this.state.anchorElement}
          onClose={this.handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}      >
          <TextField
            value={this.state.url}
            ref="url_field"
            InputProps={{
              readOnly: true,
            }}
          />

          <CopyToClipboard text={this.state.url}
            onCopy={() => this.setState({copied: true})}>
            <Button><FileCopy /></Button>
          </CopyToClipboard>

        </Popover>
      </span>
    )
  }
}

TableDataShareButton.propTypes = {
  data_table_id: PropTypes.number,
};

export default TableDataShareButton
