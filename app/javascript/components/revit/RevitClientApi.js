import axios from 'axios';

class RevitClientApi {
  constructor(baseurl) {
    this.baseurl = baseurl;
  }

  url(action) {
    let r = this.baseurl + action;
    return r;
  }

  async getCategories() {
    return await axios.get(this.url('/document/categories'));
  }

  async getInstanceParameters() {
    return await axios.get(this.url('/document/instance-parameters'));
  }

  async getTypeParameters() {
    return await axios.get(this.url('/document/type-parameters'));
  }

  async getFamilies() {
    return await axios.get(this.url('/document/families'));
  }
}

export default RevitClientApi;
