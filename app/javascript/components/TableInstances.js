import React from 'react';
import Tabulator from 'tabulator-tables';

class TableInstances extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.populateTable(this.props.data);
  }

  populateTable(data) {
    if (data == null) return;
    if (data.Instances == null) return;

    var instances = new Tabulator("#table-instances", {
      data: data.Instances,
      layout:"fitColumns",      //fit columns to width of table
      responsiveLayout:"hide",  //hide columns that dont fit on the table
      tooltips:true,            //show tool tips on cells
      addRowPos:"top",          //when adding a new row, add it to the top of the table
      history:true,             //allow undo and redo actions on the table
      // pagination:"local",       //paginate the data
      // paginationSize:20,         //allow 7 rows per page of data
      movableColumns:true,      //allow column order to be changed
      resizableRows:true,       //allow row order to be changed
      // initialSort:[             //set the initial sort order of the data
      //   {column:"Parameters.Type.Text", dir:"asc"},
      // ],
      groupStartOpen: false,
      columns: this.getInstanceColumns(data),
    });
  }

  getInstanceColumns(data) {
    var cols = [];
    data.Schema.InstanceParameters.forEach(function (p){
      var item = { title: p.Name, field: "Parameters." + p.Name + ".Text" };
      cols.push(item);
    });

    return cols;
  }

  render() {
    return <div id="table-instances"></div>;
  }
}

export default TableInstances
