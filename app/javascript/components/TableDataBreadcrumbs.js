import React from 'react';
import { Breadcrumbs, Link } from '@material-ui/core';
import { Share } from '@material-ui/icons';
import PropTypes from "prop-types";

class TableDataBreadcrumbs extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Breadcrumbs aria-label="breadcrumb">
        <Link>{this.props.data_table.project_key}</Link>
        <Link>{this.props.data_table.model_filename}</Link>
        <Link>{this.props.data_table.name}</Link>
      </Breadcrumbs>
    )
  }
}

TableDataBreadcrumbs.propTypes = {
  data_table: PropTypes.object
};

export default TableDataBreadcrumbs
