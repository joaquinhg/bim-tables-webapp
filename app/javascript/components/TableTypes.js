import React from 'react';
import Tabulator from 'tabulator-tables';

class TableTypes extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.populateTable(this.props.data);
  }

  populateTable(data) {
    if (data == null) return;
    if (data.Types == null) return;

    var types = new Tabulator("#table-types", {
      data: data.Types,
      layout:"fitColumns",      //fit columns to width of table
      responsiveLayout:"hide",  //hide columns that dont fit on the table
      tooltips:true,            //show tool tips on cells
      addRowPos:"top",          //when adding a new row, add it to the top of the table
      history:true,             //allow undo and redo actions on the table
      // pagination:"local",       //paginate the data
      // paginationSize:20,         //allow 7 rows per page of data
      movableColumns:true,      //allow column order to be changed
      resizableRows:true,       //allow row order to be changed
      // initialSort:[             //set the initial sort order of the data
      //   {column:"Parameters.Type.Text", dir:"asc"},
      // ],
      groupStartOpen: false,
      columns: this.getTypeColumns(data),
    });
  }

  getTypeColumns(data) {
    var cols = [];
    cols.push({ title: "Family", field: "FamilyName" });
    cols.push({ title: "Name", field: "Name" });
    cols.push({ title: "Instances", field: "InstanceCount" });

    data.Schema.TypeParameters.forEach(function (p){
      var item = { title: p.Name, field: "Parameters." + p.Name + ".Text" };
      cols.push(item);
    });

    return cols;
  }

  render() {
    return <div id="table-types"></div>;
  }
}

export default TableTypes
