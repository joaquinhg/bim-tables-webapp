import Parameter from 'components/data/Parameter';

class ParameterBag {
  constructor(bag) {
    this.bag = bag;
  }

  asObject(parameterDatas) {
    let row = {};

    parameterDatas.forEach((p) => {
      row[p.Name] = new Parameter(this.bag[p.Name]).Text();
    });

    return row;
  }

  getKey(parameterDatas) {
    var key = "";

    parameterDatas.forEach((p) => {
      key = key + new Parameter(this.bag[p.Name]).Text();
    });

    return key;
  }
}

export default ParameterBag
