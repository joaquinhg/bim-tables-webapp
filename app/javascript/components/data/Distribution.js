import Parameter from 'components/data/Parameter';
import ParameterBag from 'components/data/ParameterBag';

class Distribution {
  constructor(data, settings) {
    this.data = data;
    this.settings = settings;
  }

  getRows() {
    var distribution = this.computeDistribution(this.data.Instances);
    return this.createRows(distribution);
  }

  getKey(instanceData) {
    var key = new ParameterBag(instanceData.Parameters).getKey(this.settings.Parameters);
    return key;
  }

  computeDistribution(instances) {
    var distribution = {};

    instances.forEach((instance) => {
      let key = this.getKey(instance);

      if (!(key in distribution)) {
        distribution[key] = [];
      }
      distribution[key].push(instance);
    });

    return distribution;
  }

  createRows(distribution) {
    var rows = [];

    for (var key in distribution) {
      let sample = distribution[key][0];
      let count = distribution[key].length;
      let row = new ParameterBag(sample.Parameters).asObject(this.settings.Parameters);
      row.count = count;

      rows.push(row);
    }

    return rows;
  }
}

export default Distribution;
