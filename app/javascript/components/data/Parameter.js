class Parameter {
  constructor(parameterValue) {
    this.parameterValue = parameterValue;
  }

  Text() {
    if (this.parameterValue) {
      return this.parameterValue.Text;
    }
    return "";
  }

  static asObject(parameterDatas, parameterBag) {
    let row = {};

    parameterDatas.forEach((p) => {
      row[p.Name] = new Parameter(parameterBag[p.Name]).Text();
    });

    return row;
  }
}

export default Parameter
