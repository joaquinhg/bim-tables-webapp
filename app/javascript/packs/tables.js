import "stylesheets/tables.css";
import RevitClientApi from 'components/revit/RevitClientApi';

var componentRequireContext = require.context("components", true);
var ReactRailsUJS = require("react_ujs");
ReactRailsUJS.useContext(componentRequireContext);

var RevitApi = new RevitClientApi("http://localhost:3000");

export default RevitApi;
