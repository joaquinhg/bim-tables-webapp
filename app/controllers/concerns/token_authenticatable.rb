module TokenAuthenticatable
  extend ActiveSupport::Concern

  included do
    before_action :authenticate_token!
  end

  def authenticate_token!
    gon.token = '' if defined? gon
    return if user_signed_in?

    validator.validate!

    if current_user.nil?
      self.class.define_method(:current_user) { validator.user }
    end

    gon.token = token_fetcher.token if defined? gon
  end

  def api_user
    current_user
  end

protected
  def token_fetcher
    @token_fetcher ||= BearerTokenFetcher.new(request)
  end

  def validator
    @validator ||= LicenseValidator.new(code: token_fetcher.token)
  end
end
