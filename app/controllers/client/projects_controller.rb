class Client::ProjectsController < Client::ApplicationController
  before_action :fetch_summary, only: %i(show)

  def show
  end

  protected

  def fetch_summary
    @summary = ProjectSummaryFetcher.new(user: api_user, project_key: params[:id]).summary
  end
end
