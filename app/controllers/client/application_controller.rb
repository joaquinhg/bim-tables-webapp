class Client::ApplicationController < ::ApplicationController
  # Concerns
  include TokenAuthenticatable

  # Filters
  # before_action :authenticate_user!

  # Layout
  layout './layouts/client'
end
