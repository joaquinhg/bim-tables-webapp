class Client::DataSchemasController < Client::ApplicationController
  before_action :fetch_schema, only: %i(edit)

  def new
    @schema = build_schema
    authorize! :create, @schema
  end

  def edit
    authorize! :edit, @schema
  end

  protected

  def fetch_schema
    @schema = DataSchema.find(params[:id])
  end

  def build_schema
    name = "Schema name"
    custom_id = SecureRandom.uuid
    api_user.data_schemas.new(
      name: name,
      custom_id: custom_id,
      data:
      {
          CustomId: custom_id,
          Name: name,
          LanguageIso2: "en",
          Categories: [],
          Families: [],
          InstanceParameters: [],
          TypeParameters: [],
          Views: []
      }
    )
  end
end
