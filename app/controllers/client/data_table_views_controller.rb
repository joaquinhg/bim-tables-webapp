class Client::DataTableViewsController < Client::ApplicationController
  before_action :get_data_table, only: %i(show)

  def show
    authorize! :show, @data_table
  end

  protected

  def get_data_table
    @data_table = DataTable.find(params[:id])
  end
end
