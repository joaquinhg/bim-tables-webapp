class Api::DataSchemasController < Api::ApplicationController
  before_action :fetch_schema, only: %i(show update)

  def index
    @schemas = api_user.data_schemas

    render json: @schemas.map(&:data)
  end

  def create
    authorize! :create, api_user.data_schemas.new

    data = create_params[:data]
    data[:Name] = create_params[:name]

    @schema = api_user.data_schemas.create(name: create_params[:name], custom_id: create_params[:custom_id], data: data)

    render json: { id: @schema.id, url: edit_client_data_schema_url(@schema) }
  end

  def show
    authorize! :show, @schema
    render json: @schema.to_json
  end

  def update
    authorize! :edit, @schema
    @schema.update(create_params)

    head :ok
  end

  protected

  def fetch_schema
    @schema = DataSchema.find(params[:id])
  end

  def create_params
    params.require(:data_schema).permit(:name, :custom_id, data: {})
  end
end
