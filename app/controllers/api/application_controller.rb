class Api::ApplicationController < ActionController::API
  # Concerns
  include TokenAuthenticatable

  rescue_from StandardError do |e|
    respond(e.class.name, 500, e)
  end

  protected

  def respond(_error, _status, _exception)
    json = { error: _error, message: _exception.message }

    json.merge! backtrace: _exception.backtrace if Rails.env == "development"

    render json: json, status: _status
  end
end
