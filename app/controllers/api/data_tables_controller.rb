class Api::DataTablesController < Api::ApplicationController
  # skip_before_action :verify_authenticity_token

  before_action :fetch_data_table, only: %i(show url)
  before_action :fetch_revision_set, only: %i(create)

  def create
    authorize! :create, api_user.data_tables.new

    create_revision_set! if @revision_set.nil?

    data = {
      project_key: params[:project_key],
      model_filename: params[:model_filename],
      name: params[:name],
      grouping_id: params[:grouping_id],
      revision: revisions_fetcher.count + 1,
      revision_set: @revision_set,
    }

    @data_table = api_user.data_tables.new(data)
    @data_table.datafile = params[:datafile];
    @data_table.save

    render json: @data_table.to_json
  end

  def show
    authorize! :show, @data_table
    render json: @data_table.to_json
  end

  def url
    authorize! :show, @data_table
    render json: { url: schedule_share_link_url(@data_table.revision_set.uuid) }
  end

  protected

  def revision_set_key
    { project_key: params[:project_key], grouping_id: params[:grouping_id] }
  end

  def fetch_data_table
    @data_table = DataTable.find(params[:id])
  end

  def fetch_revision_set
    @revision_set = api_user.revision_sets.where(revision_set_key).first
  end

  def create_revision_set!
    @revision_set = api_user.revision_sets.create!(revision_set_key.merge uuid: SecureRandom.uuid)
  end

  def revisions_fetcher
    @revisions_fetcher ||= RevisionsFetcher.new(revision_set_key)
  end
end
