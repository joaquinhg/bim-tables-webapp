class Admin::PluginInstallersController < Admin::ApplicationController
  before_action :fetch_installer, only: %i(show)

  def index
    @installers = PluginInstaller.all
  end

  def new
    @installer = PluginInstaller.new
  end

  def create
    @installer = PluginInstaller.create(installer_create_params)
    @installer.save!

    redirect_to admin_plugin_installer_path(@installer)
  end

  def show
  end

protected

  def fetch_installer
    @installer = PluginInstaller.find(params[:id])
  end

  def installer_create_params
    params.require(:plugin_installer).permit(%i(version release assetfile))
  end

end
