class Admin::ApplicationController < ::ApplicationController
  # Filters
  before_action :authenticate_user!
  before_action :ensure_admin_user

  # Layout
  layout './admin/layouts/application'

  def home
  end

protected

  def ensure_admin_user
    authorize! :access, self
  end
end
