class Admin::PluginBundlesController < Admin::ApplicationController
  before_action :fetch_bundle, only: %i(show)

  def index
    @bundles = PluginBundle.all
  end

  def new
    @bundle = PluginBundle.new
  end

  def create
    @bundle = PluginBundle.create(bundle_create_params)
    @bundle.save!

    redirect_to admin_plugin_bundle_path(@bundle)
  end

  def show
  end

protected

  def fetch_bundle
    @bundle = PluginBundle.find(params[:id])
  end

  def bundle_create_params
    params.require(:plugin_bundle).permit(%i(version release assetfile))
  end

end
