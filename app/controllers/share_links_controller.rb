class ShareLinksController < ApplicationController
  before_action :fetch_revision_set, only: %i(schedule revision)

  def schedule
    @latest = @revision_set.data_tables.latest.first
    @revision_ids = @revision_set.data_tables.select(:id, :revision)
  end

  def revision
    @revision = @revision_set.data_tables.where(revision: params[:number]).first

    render json: @revision.to_json
  end

  protected

  def fetch_revision_set
    @revision_set = RevisionSet.friendly.find(params[:id])
  end
end
