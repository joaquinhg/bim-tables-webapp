class DownloadsController < ApplicationController
  # Filters
  before_action :authenticate_user!, only: %i(token installer)

  def plugin
    code = params[:code]

    license = License.active.where(code: code).first
    return head :not_found unless license

    bundle = PluginBundle.latest.where(release: license.release).first
    return head :not_found unless bundle

    send_file(Rails.root.join(bundle.assetfile.current_path),
      filename: bundle.assetfile.identifier,
      type: bundle.assetfile.content_type
    )
  end

  def installer
    installer = PluginInstaller.latest.where(release: params[:release]).first

    return head :not_found unless installer

    authorize! :download, installer
    send_file(Rails.root.join(installer.assetfile.current_path),
      filename: installer.assetfile.identifier,
      type: installer.assetfile.content_type
    )
  end

  def token
    license = License.latest.active.where(user: current_user, release: params[:release]).first
    return head :not_found unless license

    data = { 'Url': root_url, 'Code': license.code }
    send_data(data.to_json,
      filename: 'plugindata',
      type: :json
    )
  end
end
