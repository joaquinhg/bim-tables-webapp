class Dashboard::DashboardsController < Dashboard::ApplicationController
  before_action :fetch_license, only: %i(index)

  def index
  end

  protected

  def fetch_license
    @license = current_user.default_license
  end
end
