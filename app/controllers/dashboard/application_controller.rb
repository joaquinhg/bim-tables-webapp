class Dashboard::ApplicationController < ::ApplicationController
  # Filters
  before_action :authenticate_user!

  # Layout
  layout './layouts/dashboard'
end
