class EmulatorController < ApplicationController
  def categories
    cats =
    [
      { Id: "1", Name: "Walls" },
      { Id: "2", Name: "Doors" },
      { Id: "3", Name: "Windows" }
    ]

    render json: cats.to_json
  end

  def families
    families =
    [
      { Id: "1", Name: "Dimensional Lumber" },
      { Id: "2", Name: "Dimensional Lumber - Column" },
    ]

    render json: families.to_json
  end

  def instance_parameters
    parameters =
    [
      { Id: "1", Name: "Frame Material", type: "Text" },
    ]

    render json: parameters.to_json
  end

  def type_parameters
    parameters =
    [
      { Id: "1", Name: "Height", type: "Lenght" },
      { Id: "2", Name: "Width", type: "Lenght" },
    ]

    render json: parameters.to_json
  end
end
