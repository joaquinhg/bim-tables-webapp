# frozen_string_literal: true

class Ability
  include CanCan::Ability

  # Store instance methods to be ran on initialization
  def self.abilities_methods
    @abilities_methods ||= []
  end

  # Abilities
  include AdminAbility
  include UserAbility

  def initialize(user)
    @user = user || User.new
    return unless @user.is_a?(User)

    # Inject each abilities module into the class
    Ability.abilities_methods.each { |method| instance_exec(&method) }
  end
end
