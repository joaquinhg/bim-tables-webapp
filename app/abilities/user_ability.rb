module UserAbility
  extend ActiveSupport::Concern

  included do
    abilities_methods << lambda do
      # can :edit, User do |target|
      #   target == @user
      # end

      can :download, PluginInstaller do |target|
        !@user.blocked_at?
      end

      can :create, DataSchema do |target|
        true
      end

      can :show, DataSchema do |target|
        target.user_id == @user.id
      end

      can :edit, DataSchema do |target|
        target.user_id == @user.id
      end

      can :create, DataTable do |target|
        true
      end

      can :show, DataTable do |target|
        target.user_id == @user.id
      end
    end
  end
end
