module AdminAbility
  extend ActiveSupport::Concern

  included do
    abilities_methods << lambda do
      if @user.admin?
        can :access, Admin::ApplicationController
      end
    end
  end
end
