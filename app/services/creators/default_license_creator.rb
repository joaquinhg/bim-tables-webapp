class DefaultLicenseCreator
  DEFAULT_RELEASE = "beta".freeze

  def initialize(user:)
    @user = user
  end

  def create_if_missing!
    get_license || create!
  end

  protected

  def gen_code
    SecureRandom.uuid
  end

  def create!
    @user.licenses.create(release: DEFAULT_RELEASE, code: gen_code, active: true)
  end

  def get_license
    @user.licenses.where(release: DEFAULT_RELEASE).first
  end
end
