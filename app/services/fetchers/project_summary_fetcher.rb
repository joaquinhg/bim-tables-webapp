class ProjectSummaryFetcher
  def initialize(user:, project_key:)
    @user = user
    @project_key = project_key
  end

  def summary
    @summary ||= fetch_summary
  end

  protected

  def fetch_summary
    {
      key: @project_key,
      revision_sets: fetch_revision_sets
    }
  end

  def fetch_revision_sets
    @user.revision_sets.includes(:latest).where(project_key: @project_key).map do |rs|
      rs.as_json.merge latest: rs.latest
    end
  end
end
