class RevisionsFetcher
  def initialize(project_key:, grouping_id:)
    @project_key = project_key
    @grouping_id = grouping_id
  end

  def count
    revisions.count
  end

  def revisions
    @revisions ||= DataTable.where(project_key: @project_key, grouping_id: @grouping_id)
  end

  def latest
    @latest ||= revisions.latest.first
  end
end
