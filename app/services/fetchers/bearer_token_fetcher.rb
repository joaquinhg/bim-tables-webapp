class BearerTokenFetcher
  # Errors
  TokenNotPresent = Class.new(StandardError)
  TokenNotValid = Class.new(StandardError)

  KEY = "Authorization".freeze
  TYPE = "Bearer".freeze

  def initialize(request)
    @request = request
  end

  def token
    @token ||= begin
      raise TokenNotPresent if !@request.headers.key?(KEY)

      value = @request.headers[KEY]

      raise TokenNotValid if value.blank? || !value.start_with?(TYPE)

      value.delete_prefix(TYPE).strip
    end
  end
end
