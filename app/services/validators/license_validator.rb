class LicenseValidator
  # Errors
  LicenseNotFound = Class.new(StandardError)
  LicenseNotActive = Class.new(StandardError)
  UserIsBlocked = Class.new(StandardError)
  UserIsLocked = Class.new(StandardError)

  def initialize(code:)
    @code = code
  end

  def validate!
    license.user && user
  end

  def user
    @user ||= begin
      user = license.user

      raise UserIsBlocked if user.blocked_at?
      raise UserIsLocked if user.locked_at?

      user
    end
  end

protected
  def license
    @license ||= begin
      license = License.where(code: @code).first

      raise LicenseNotFound unless license
      rails LicenseNotActive unless license.active

      license
    end
  end

end
